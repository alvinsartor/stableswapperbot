# Stable Swapper Bot

Stable Swapper is a Bot that swaps stablecoins and aims at generating profit by exploiting the small volatility between them.

## Basic Idea

The goal of all Stablecoins is to maintain their own value as close as possible to the value of a currency (i.e. USD). This is obtained through a store of value (i.e. cash or similar stored somewhere) or algorithms that aim at compensate deltas through mathematical mechanism or seigniorage.

We all know, though, that the market doesn't like stability and will sprinkle entropy whenever it is possible. This causes continues small oscillations that can become quite considerable from time to time.

The bot tries to jump between a token and the other, exploting these small oscillations to gain few cents each transaction. Of course this is possible only if the transaction fees are very low and Polygon is thus the perfect platform to do so!

## Functioning

The project is made of 2 parts: the website and a cronjob.

### Website

The [website](https://stableswapper.herokuapp.com/) shows the current state and allows to study how the bot is behaving, so to improve it over time.

![website](readme_resources/preview.png)

It also has an [endpoint](https://stableswapper.herokuapp.com/trigger-swap/) that, if called, triggers the checks and eventually the swaps over stablecoins.

### Cronjob

As heroku didn't provide decent cronjobs, I relied on an [external website](https://console.cron-job.org/). Here I just set a recurring job executed every minute that simply calls the endpoint that triggers the swaps.

![cronjob](readme_resources/cronjob.png)
