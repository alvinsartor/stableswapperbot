import os
import json
import posixpath
import mimetypes
import django_heroku

mimetypes.add_type("text/css", ".css", True)

# Important paths
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRETS_DIR = os.path.join(BASE_DIR, '.secrets')

# Addresses for error mails
DEFAULT_ADMIN_EMAIL = 'alvinsartor@gmail.com'
ADMINS = [('Alvin', 'alvinsartor@gmail.com')]
SERVER_EMAIL = 'alvinsartor@gmail.com'

# Load environment variables
ENVIRONMENT_VARS_FILE = os.path.join(SECRETS_DIR, 'environment.json')
if os.path.exists(ENVIRONMENT_VARS_FILE):
    # IF File exists, we get them from there,
    with open(ENVIRONMENT_VARS_FILE, 'r') as env_file:
        data = env_file.read()

    ENVIRONMENT_VARIABLES = json.loads(data)
    DEBUG = ENVIRONMENT_VARIABLES.get('DEBUG', 'True') == 'True'
    SECRET_KEY = ENVIRONMENT_VARIABLES['SECRET_KEY']
    WALLET_PRIVATE_KEY = ENVIRONMENT_VARIABLES['WALLET_PRIVATE_KEY']
    WALLET_ADDRESS = ENVIRONMENT_VARIABLES['WALLET_ADDRESS']
    DB_NAME = ENVIRONMENT_VARIABLES['DB_NAME']
    DB_USER = ENVIRONMENT_VARIABLES['DB_USER']
    DB_HOST = ENVIRONMENT_VARIABLES['DB_HOST']
    DB_PORT = ENVIRONMENT_VARIABLES['DB_PORT']
    DB_PASSWORD = ENVIRONMENT_VARIABLES['DB_PASSWORD']
    DEFAULT_ADMIN_PASSWORD = ENVIRONMENT_VARIABLES['DEFAULT_ADMIN_PASSWORD']
    FAST_NODE_RPC_URL = ENVIRONMENT_VARIABLES['FAST_NODE_RPC_URL']

    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
else:
    # ELSE from ENV
    DEBUG = os.environ.get('DEBUG', 'False') == 'True'
    SECRET_KEY = os.environ.get('SECRET_KEY', None)
    WALLET_PRIVATE_KEY = os.environ.get('WALLET_PRIVATE_KEY', None)
    WALLET_ADDRESS = os.environ.get('WALLET_ADDRESS', None)
    DB_NAME = os.environ.get('DB_NAME', None)
    DB_USER = os.environ.get('DB_USER', None)
    DB_HOST = os.environ.get('DB_HOST', None)
    DB_PORT = os.environ.get('DB_PORT', None)
    DB_PASSWORD = os.environ.get('DB_PASSWORD', None)
    DEFAULT_ADMIN_PASSWORD = os.environ.get('DEFAULT_ADMIN_PASSWORD', None)
    FAST_NODE_RPC_URL = os.environ.get('FAST_NODE_RPC_URL', None)

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = os.environ.get('MAILGUN_SMTP_SERVER', '')
    EMAIL_PORT = os.environ.get('MAILGUN_SMTP_PORT', '')
    EMAIL_HOST_USER = os.environ.get('MAILGUN_SMTP_LOGIN', '')
    EMAIL_HOST_PASSWORD = os.environ.get('MAILGUN_SMTP_PASSWORD', '')

print("Debug mode: %s" % DEBUG)

if not WALLET_ADDRESS:
    raise Exception('Could not load environment variables')

# Allowed hosts
ALLOWED_HOSTS = ['localhost', 'stableswapper.herokuapp.com']

# Application references
INSTALLED_APPS = [
    'bot',
    'kernel',
    'wallet',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

# Middleware framework
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'StableSwapper.urls'

# Template configuration
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'StableSwapper.wsgi.application'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = posixpath.join(*(BASE_DIR.split(os.path.sep) + ['static']))

django_heroku.settings(locals())
