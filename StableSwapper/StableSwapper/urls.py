# Django
from django.urls import path
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import RedirectView

# Local
from bot import views
from bot.views import data as data_views
from bot.views import actions as action_views

# fmt: off
urlpatterns = [
    # Admin
    path('admin/', admin.site.urls, name="admin"),

    # Website
    path('', RedirectView.as_view(url='/home/')),
    path('home/', views.HomeView.as_view(), name="home"),
    path('statistics/', views.StatisticsView.as_view(), name="statistics"),
    path('checks/', views.ChecksView.as_view()),
    path('transactions/', views.TransactionsView.as_view()),

    # Actions
    path('action/trigger-swap/', action_views.SwapTriggerView.as_view()),
    path('action/manage-checks-runner/', action_views.ManageChecksRunnerTriggerView.as_view()),
    path('action/delete-old-checks/', action_views.OldChecksDeleteTriggerView.as_view()),
    path('action/confirm-transactions/', action_views.ConfirmTransactionTriggerView.as_view()),
    path('action/add-daily-statistic/', action_views.AddDailyStatisticTriggerView.as_view()),
    path('action/test-email/', action_views.TestMailTriggerView.as_view()),
    path('action/alert-if-low-in-matic/', action_views.AlertIfLowInMaticTriggerView.as_view()),
    path('action/modify-bots-funds/<str:amount>/', action_views.ModifyBotFundsTriggerView.as_view()),

    # Data Home Page
    path('data/amount_over_time/', data_views.DataAmountOverTimeView.as_view()),

    # Data Statistics - Daily
    path('data/daily_apr_over_time/', data_views.DataDailyAprOverTimeView.as_view()),
    path('data/daily_profit_over_time/', data_views.DataDailyProfitOverTimeView.as_view()),
    path('data/checks_durations_over_time/', data_views.DataCheckDurationOverTimeView.as_view()),
    path('data/checks_trends_over_time/', data_views.DataCheckTrendsOverTimeView.as_view()),
    path('data/transactions_trends_over_time/', data_views.DataTransactionTrendsOverTimeView.as_view()),
    path('data/transactions_positive_vs_negative_count/', data_views.DataTransactionPositiveVsNegativeCountView.as_view()),
    path('data/transactions_positive_vs_negative_amount/', data_views.DataTransactionPositiveVsNegativeAmountView.as_view()),
    path('data/transactions_successful_vs_not/', data_views.DataTransactionSuccessfulVsNotView.as_view()),
    path('data/daily_fees/', data_views.DataFeesOverTimeView.as_view()),

    # Data Statistics - Transactions
    path('data/transactions_delays/', data_views.DataTransactionDelaysView.as_view()),
    path('data/transactions_gas_price/', data_views.DataTransactionGasPriceView.as_view()),
    path('data/transactions_slippage/', data_views.DataTransactionSlippageView.as_view()),

    # Data Statistics - Checks
    path('data/failures_over_time/', data_views.DataFailuresOverTimeView.as_view()),
    path('data/profit_over_time/', data_views.DataProfitOverTimeView.as_view()),
    path('data/tokens_owned/', data_views.DataTokensOwnedView.as_view()),
    path('data/tokens_desired/', data_views.DataTokensDesiredView.as_view()),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
# fmt: on
