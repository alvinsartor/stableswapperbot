# Python
from typing import List, Dict, Any, Optional
import concurrent.futures
import time

# 3rd Party
import requests

# Django
from django.conf import settings

# Local
from kernel.models import Token
from wallet.token_with_amount import TokenWithAmount
from wallet.quote_with_tx_data import QuoteWithTxData
from wallet.web3_instance import w3, fast_node_w3


class ExchangeProxyV2:
    @staticmethod
    def get_best_swap_quote(
        token_with_amount: TokenWithAmount, swappable_tokens: List[Token], gas_price: float, slippage: float
    ) -> QuoteWithTxData:

        print(" - - Starting quote for %s - - " % token_with_amount.token.symbol)
        start = time.time()

        nonce = fast_node_w3.eth.get_transaction_count(settings.WALLET_ADDRESS, 'pending')
        quotes: List[Optional[QuoteWithTxData]] = []

        def _get_quote_and_append_result(token: Token) -> None:
            quotes.append(ExchangeProxyV2._get_raw_transaction(token_with_amount, token, gas_price, slippage, nonce))

        with concurrent.futures.ThreadPoolExecutor(max_workers=15) as executor:
            executor.map(_get_quote_and_append_result, swappable_tokens)

        non_failed_quotes = [quote for quote in quotes if quote]
        print(" + + Ended quote for %s (elapsed: %s) + + " % (token_with_amount.token.symbol, time.time() - start))

        if non_failed_quotes:
            return sorted(non_failed_quotes, key=lambda q: -q.token_with_amount.adjusted_amount)[0]

        raise Exception('All quotes failed for %s' % token_with_amount.token.symbol)

    @staticmethod
    def _get_raw_transaction(
        token_from: TokenWithAmount, token_to: Token, gas_price: float, slippage: float, nonce: int,
    ) -> Optional[QuoteWithTxData]:
        print("Starting quote from %s to %s" % (token_from.token.symbol, token_to.symbol))
        start = time.time()

        gas_price_in_gwei = gas_price * pow(10, 9)
        params = {
            'fromTokenAddress': token_from.token.address,
            'toTokenAddress': token_to.address,
            'amount': f"{token_from.amount:.0f}",  # suppress scientific notation
            'slippage': slippage,
            'fromAddress': settings.WALLET_ADDRESS,
            'allowPartialFill': True,
            'gasPrice': f"{gas_price_in_gwei:.0f}",
        }
        response = requests.get('https://api.1inch.exchange/v3.0/137/swap', params=params)
        if response.status_code != 200:
            # raise Exception(response.json()['message'])
            return None
        response_json = response.json()
        
        # prepare token with amount
        quote = float(response_json['toTokenAmount'])
        token_with_amount = TokenWithAmount(token_to, quote)

        # prepare quote with tx data
        tx_data = response_json['tx']
        ExchangeProxyV2._prepare_transaction_for_submission(tx_data, nonce)
        print(
            "Ending quote from %s to %s (elapsed: %s)"
            % (token_from.token.symbol, token_to.symbol, time.time() - start)
        )
        return QuoteWithTxData(token_with_amount, tx_data)

    @staticmethod
    def _prepare_transaction_for_submission(tx: Dict[str, Any], nonce):
        """
            This function is needed as it seems that the tx that 1inch returns
            has to be massaged a bit to get it ready for submission.
        """
        tx.update(
            {
                'nonce': nonce,
                'to': w3.toChecksumAddress(tx['to']),
                'value': int(tx['value']),
                'gasPrice': int(tx['gasPrice']),
                'chainId': 137,
            }
        )

    @staticmethod
    def sign_and_submit(quote: QuoteWithTxData) -> str:
        # sign and send transaction
        signed_transaction = fast_node_w3.eth.account.sign_transaction(quote.tx_data, settings.WALLET_PRIVATE_KEY)

        if settings.DEBUG:
            return "0x00"

        tx_address = fast_node_w3.eth.send_raw_transaction(signed_transaction.rawTransaction)
        return tx_address.hex()

    @staticmethod
    def swap_for_MATIC(token_from: TokenWithAmount, gas_price: float) -> Optional[str]:
        nonce = fast_node_w3.eth.get_transaction_count(settings.WALLET_ADDRESS, 'pending')
        MATIC = Token(
            symbol='MATIC',
            address='0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE',
            decimals=18,
            maximum_risk_allowed=0,
            disabled=True,
            comment='Temporary MATIC token. Do NOT save.',
        )

        slippage = 0.05

        quote = ExchangeProxyV2._get_raw_transaction(token_from, MATIC, gas_price, slippage, nonce)
        return ExchangeProxyV2.sign_and_submit(quote) if quote else None
