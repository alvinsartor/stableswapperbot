# Python
from typing import List
import random

# Django
from django.conf import settings

# 3rd Party
from web3 import Web3
from web3.middleware import geth_poa_middleware


rpc_urls = [
    'https://rpc-mainnet.matic.quiknode.pro',
    'https://rpc-mainnet.matic.network',
    #'https://rpc.polycat.finance',
    #'https://rpc-mainnet.maticvigil.com',
    #'https://matic-mainnet-full-rpc.bwarelabs.com ',
    #'https://matic-mainnet.chainstacklabs.com',
]

clients: List[Web3] = []
for rpc_url in rpc_urls:
    client = Web3(Web3.HTTPProvider(rpc_url))
    if client.isConnected():
        clients.append(client)

if not clients:
    raise Exception('No Web3 clients available')

w3 = random.choice(clients)

fast_note_rpc = settings.FAST_NODE_RPC_URL
fast_node_w3 = Web3(Web3.WebsocketProvider(fast_note_rpc))
if not fast_node_w3.isConnected():
    raise Exception('Could not connect to fast node client')

fast_node_w3_non_critical = Web3(Web3.WebsocketProvider(fast_note_rpc))
if not fast_node_w3_non_critical.isConnected():
    raise Exception('Could not connect to fast node client')


# This line is necessary to be able to use w3.eth.get_block(block_number)
# No idea why!
w3.middleware_onion.inject(geth_poa_middleware, layer=0)
fast_node_w3_non_critical.middleware_onion.inject(geth_poa_middleware, layer=0)


print("Basic node: %s" % w3.provider.endpoint_uri)
print("Fast node: %s" % fast_node_w3.provider.endpoint_uri)
print("Fast node 2: %s" % fast_node_w3_non_critical.provider.endpoint_uri)
