# Django
from django.contrib import admin

# Local
from wallet.models import SwapperSettings
from wallet.models import ChecksRunnerSession


@admin.register(SwapperSettings)
class SwapperSettingsAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')


@admin.register(ChecksRunnerSession)
class ChecksRunnerSessionAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'shutdown_date', 'check_batches_triggered', 'should_shutdown')
