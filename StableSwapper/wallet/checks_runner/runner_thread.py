# Python
import threading

# Django
from django.utils import timezone

# Local
from wallet.swapper_v3 import SwapperV3
from wallet.models import ChecksRunnerSession
from wallet.models import SwapperSettings


class RunnerThread(threading.Thread):
    def __init__(self, session_id: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.session_id = session_id

    def run(self) -> None:
        session = ChecksRunnerSession.objects.get(id=self.session_id)
        iterations_before_shutdown = int(SwapperSettings.get_swapper_settings('CHECK_RUNNER_ITERATIONS'))

        try:
            while iterations_before_shutdown > 0:
                session.refresh_from_db()
                if session.should_shutdown:
                    break

                SwapperV3.swap_if_possible()

                session.refresh_from_db()
                session.last_check_date = timezone.now()
                session.check_batches_triggered += 1
                session.save()

                iterations_before_shutdown -= 1
        except Exception as error:
            session.error = str(error)

        session.shutdown_date = timezone.now()
        session.save()
