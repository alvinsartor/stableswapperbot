# Python
import concurrent.futures
import math

# Django
from typing import List
from django.conf import settings

# 3rd Party
from web3 import Web3

# Local
from wallet.web3_instance import w3
from wallet.token_with_amount import TokenWithAmount
from kernel.models import Token

tokenABI = [
    {
        "constant": "true",
        "inputs": [{"name": "_owner", "type": "address"}],
        "name": "balanceOf",
        "outputs": [{"name": "balance", "type": "uint256"}],
        "type": "function",
    },
    {
        "constant": "true",
        "inputs": [],
        "name": "decimals",
        "outputs": [{"name": "", "type": "uint8"}],
        "type": "function",
    },
]


class WalletChecker:
    @staticmethod
    def get_tokens_amounts() -> List[TokenWithAmount]:
        result: List[TokenWithAmount] = []

        def fetch_token_and_add_to_list(token: Token) -> None:
            quote = TokenWithAmount(token, WalletChecker.get_single_token_amount(token))
            result.append(quote)

        with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
            executor.map(fetch_token_and_add_to_list, Token.objects.filter(disabled=False))

        return result

    @staticmethod
    def get_single_token_amount(token: Token) -> float:
        checksum_address = Web3.toChecksumAddress(token.address)
        contract = w3.eth.contract(address=checksum_address, abi=tokenABI)
        balance = contract.functions.balanceOf(settings.WALLET_ADDRESS).call()
        return balance

    @staticmethod
    def get_total_owned() -> float:
        owned_tokens = WalletChecker.get_tokens_amounts()
        return sum([token.adjusted_amount for token in owned_tokens])

    @staticmethod
    def get_matic_balance() -> float:
        balance = w3.eth.getBalance(settings.WALLET_ADDRESS)
        return balance / math.pow(10, 18)
