# Python
from typing import Dict, Any

# Local
from wallet.token_with_amount import TokenWithAmount


class QuoteWithTxData:
    def __init__(self, token_with_amount: TokenWithAmount, tx_data: Dict[str, Any]):
        self.token_with_amount = token_with_amount
        self.tx_data = tx_data
