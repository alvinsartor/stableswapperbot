# Python
from datetime import datetime

# Django
from typing import Dict, Any, Optional
from django.utils.timezone import make_aware

# 3rd Party
import pytz
from web3.exceptions import TransactionNotFound

# Local
from wallet.web3_instance import fast_node_w3_non_critical


class TransactionsChecker:
    @staticmethod
    def get_transaction_receipt(hex: str) -> Optional[Dict[str, Any]]:
        try:
            return fast_node_w3_non_critical.eth.get_transaction_receipt(hex)
        except TransactionNotFound:
            return None

    @staticmethod
    def was_transaction_executed(tx_receipt: Dict[str, Any]) -> bool:
        return tx_receipt['status'] == 1

    @staticmethod
    def get_transaction_block(tx_receipt: Dict[str, Any]) -> int:
        return tx_receipt['blockNumber']

    @staticmethod
    def get_transaction_gas_used(tx_receipt: Dict[str, Any]) -> int:
        return tx_receipt['gasUsed']

    @staticmethod
    def get_transaction_time(tx_receipt: Dict[str, Any]) -> datetime:
        block_number = TransactionsChecker.get_transaction_block(tx_receipt)
        unix_timestamp = fast_node_w3_non_critical.eth.getBlock(block_number).timestamp
        tx_datetime = datetime.fromtimestamp(unix_timestamp)
        return make_aware(tx_datetime, timezone=pytz.timezone("UTC"))
