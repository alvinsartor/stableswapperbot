# Python
from typing import List, Dict, Optional
import time
import json

# 3rd Party
import requests

# Local
from kernel.models import Token
from kernel.models import Transaction
from wallet.wallet_checker import WalletChecker
from wallet.token_with_amount import TokenWithAmount
from wallet.exchange_proxy_v2 import ExchangeProxyV2
from wallet.models import SwapperSettings
from wallet.web3_instance import fast_node_w3
from wallet.quote_with_tx_data import QuoteWithTxData


class SwapperV3:
    @staticmethod
    def swap_if_possible() -> bool:
        # If the network is too busy, we step out and wait a bit to prevent wasting money
        if SwapperV3._should_bot_pause():
            time.sleep(5)
            return False

        owned_tokens: List[TokenWithAmount] = WalletChecker.get_tokens_amounts()
        total_owned = sum([token.adjusted_amount for token in owned_tokens])
        minimum_swappable = float(SwapperSettings.get_swapper_settings('MINIMUM_SWAPPABLE'))

        swappable_tokens = [t for t in owned_tokens if t.adjusted_amount >= minimum_swappable]
        swappable_tokens = sorted(swappable_tokens, key=lambda tk: -tk.adjusted_amount)

        # By always swapping a fixed amount we expose ourselves to more opportunities, reduce slippage issues
        # and have less chance of cornering ourselves in a bad swap
        maximum_swappable_amount = float(SwapperSettings.get_swapper_settings('MAXIMUM_SWAPPABLE_AMOUNT'))

        # Calculate how much token is allowed to receive to not pose too much risk on the portfolio
        tokens_allowances: Dict[str, float] = SwapperV3._get_allowances(owned_tokens, total_owned)

        # calculate gas price and slippage
        gas_price = SwapperV3._get_gas_price()
        slippage = float(SwapperSettings.get_swapper_settings('SLIPPAGE'))

        has_swapped = False
        for token_to_swap in swappable_tokens:
            swappable_amount = min(token_to_swap.adjusted_amount, maximum_swappable_amount)
            token_to_swap.set_amount_from_adjusted(swappable_amount)
            swapped_to = SwapperV3._swap_single_token_if_possible(
                token_to_swap, total_owned, tokens_allowances, gas_price, slippage
            )

            # If something got swapped, let's modify the allowances
            if swapped_to:
                tokens_allowances[swapped_to.token.symbol] -= swapped_to.adjusted_amount
                has_swapped = True

        return has_swapped

    @staticmethod
    def _swap_single_token_if_possible(
        token_to_swap: TokenWithAmount,
        total_owned: float,
        tokens_allowances: Dict[str, float],
        gas_price: float,
        slippage: float,
    ) -> Optional[TokenWithAmount]:
        from bot.models import Check

        check = Check()
        start_time = time.time()

        try:
            # store token to swap into check
            check.total_owned = total_owned
            check.token_to_swap_from = token_to_swap.token
            check.amount_to_swap_from = token_to_swap.adjusted_amount

            # get the list of tokens we can swap to considering how much they already have and their risk factor
            # if a swap of token_to_swap.amount would get past the token allowance, we discard it
            other_tokens = Token.objects.exclude(symbol=token_to_swap.token.symbol).exclude(disabled=True)
            swappable_tokens = [
                token
                for token in other_tokens
                if tokens_allowances.get(token.symbol, 0) > token_to_swap.adjusted_amount
            ]

            # get quote for allowed token
            quote_with_tx_data: QuoteWithTxData = ExchangeProxyV2.get_best_swap_quote(
                token_to_swap, swappable_tokens, gas_price, slippage
            )
            token_to = quote_with_tx_data.token_with_amount
            check.token_to_swap_to = token_to.token
            check.amount_to_swap_to = token_to.adjusted_amount

            # calculate forecasted profits
            forecasted_profit = token_to.adjusted_amount - token_to_swap.adjusted_amount
            swap_threshold = float(SwapperSettings.get_swapper_settings('SWAP_THRESHOLD'))
            aborted = forecasted_profit < swap_threshold
            check.raw_data = json.dumps(quote_with_tx_data.tx_data)
            check.forecasted_profit = forecasted_profit
            check.aborted = aborted

            if aborted:
                check.duration_seconds = time.time() - start_time
                check.save()
                return None

            # Execute transaction
            tx_address = ExchangeProxyV2.sign_and_submit(quote_with_tx_data)

            # store successful transaction (it will be checked via a cronjob)
            transaction = Transaction.objects.create(
                total_owned_at_creation=total_owned,
                token_from=token_to_swap.token,
                token_to=token_to.token,
                amount_from=token_to_swap.adjusted_amount,
                amount_to=token_to.adjusted_amount,
                address=tx_address,
                endpoint_uri=fast_node_w3.provider.endpoint_uri[:40],
                gas_price=gas_price,
                slippage=slippage,
            )
            check.transaction = transaction

        except Exception as error:
            check.error = str(error)
            token_to = None

        check.duration_seconds = time.time() - start_time
        check.save()
        return token_to

    @staticmethod
    def _get_gas_price() -> float:
        max_gas_price = float(SwapperSettings.get_swapper_settings('MAXIMUM_GAS_PRICE'))
        try:
            response = requests.get('https://gasstation-mainnet.matic.network').content
            prices = json.loads(response)
            fast_price = float(prices['fast'])
            return min(fast_price + 0.25, max_gas_price)
        except Exception:
            return max_gas_price

    @staticmethod
    def _get_allowances(owned_tokens: List[TokenWithAmount], total_owned: float) -> Dict[str, float]:
        """
            Calculate the total amount each token can still be swapped into, considering their risk factor
            and how much % they already represent. We add a 10% for not excluding tokens whenever
            the swapped amount is just above their maximum allowance
        """

        tokens_allowances: Dict[str, TokenWithAmount] = {}
        for token in owned_tokens:
            maximum_allowed = token.token.maximum_risk_allowed * total_owned - token.adjusted_amount
            generous_addition = maximum_allowed * 1.1
            truncated = TokenWithAmount.truncate_adjusted_amount_at_second_digit(generous_addition)
            tokens_allowances[token.token.symbol] = truncated

        return tokens_allowances

    @staticmethod
    def _should_bot_pause() -> bool:
        """
            In certain conditions (i.e. network very busy) it is detrimental to leave the bot operate
            as it tends to send tons of transactions that will never be executed and will cost money.

            In these cases, we let the bot rest for a bit.
        """

        return Transaction.objects.filter(state=Transaction.States.WAITING).count() > 10

    @staticmethod
    def top_up_MATIC() -> Optional[str]:
        owned_tokens: List[TokenWithAmount] = WalletChecker.get_tokens_amounts()
        most_owned_token: TokenWithAmount = sorted(owned_tokens, key=lambda tk: -tk.adjusted_amount)[0]

        # we just swap 1 $ of the most owned token
        most_owned_token.set_amount_from_adjusted(1)
        gas_price = SwapperV3._get_gas_price()
        return ExchangeProxyV2.swap_for_MATIC(most_owned_token, gas_price)
