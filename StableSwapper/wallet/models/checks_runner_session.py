# Python
from datetime import timedelta

# Django
from django.db import models
from django.utils import timezone


class ChecksRunnerSession(models.Model):
    start_date = models.DateTimeField(auto_now_add=True)
    shutdown_date = models.DateTimeField(blank=True, null=True, default=None)
    last_check_date = models.DateTimeField(blank=True, null=True, default=None)
    check_batches_triggered = models.IntegerField(default=0)
    should_shutdown = models.BooleanField(default=False)
    error = models.TextField(blank=True)

    @property
    def duration_in_seconds(self):
        if not self.shutdown_date:
            return -1
        return (self.shutdown_date - self.start_date).total_seconds()

    @property
    def is_runner_idle(self):
        date = self.last_check_date or self.start_date
        two_mins_ago = timezone.now() - timedelta(seconds=60)
        return date < two_mins_ago
