# Python
from typing import Dict

# Django
from django.db import models


class SwapperSettings(models.Model):
    name = models.CharField(max_length=64)
    value = models.CharField(max_length=64)

    DEFAULT_SETTINGS = {
        'SWAP_THRESHOLD': '0.1',
        'MINIMUM_SWAPPABLE': '20.0',
        'MAXIMUM_SWAPPABLE_AMOUNT': '100.0',
        'MAXIMUM_GAS_PRICE': '10',
        'SLIPPAGE': '0.1',
        'CHECK_RUNNER_ITERATIONS': '1000',
    }

    @staticmethod
    def get_swapper_settings(setting_name: str) -> Dict[str, str]:
        try:
            return SwapperSettings.objects.get(name=setting_name).value
        except SwapperSettings.DoesNotExist:
            default_value = SwapperSettings.DEFAULT_SETTINGS[setting_name]
            SwapperSettings.objects.create(name=setting_name, value=default_value)
            return default_value
