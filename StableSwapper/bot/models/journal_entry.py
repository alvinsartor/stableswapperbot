# Django
from django.db import models


class JournalEntry(models.Model):
    date = models.DateField()
    title = models.CharField(max_length=256)
    goal = models.CharField(max_length=256, blank=True, null=True, default=None)
    additional_info = models.TextField(blank=True, null=True, default=None)
