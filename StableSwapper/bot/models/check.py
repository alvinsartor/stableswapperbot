# Django
from django.db import models

# Local
from kernel.models import Token
from kernel.models import Transaction


class Check(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    duration_seconds = models.FloatField(blank=True, null=True)
    total_owned = models.FloatField(blank=True, null=True)
    token_to_swap_from = models.ForeignKey(Token, on_delete=models.CASCADE, related_name='check_from', null=True)
    amount_to_swap_from = models.FloatField(blank=True, null=True)
    token_to_swap_to = models.ForeignKey(Token, on_delete=models.CASCADE, related_name='check_to', null=True)
    amount_to_swap_to = models.FloatField(blank=True, null=True)
    forecasted_profit = models.FloatField(blank=True, null=True)
    aborted = models.BooleanField(blank=True, null=True)
    transaction = models.OneToOneField(Transaction, on_delete=models.CASCADE, null=True, related_name="bot_check")
    error = models.TextField(blank=True, null=True)
    raw_data = models.TextField(blank=True, default='')

    @property
    def is_successful(self):
        return not self.error
