# Django
from django.db import models


class DailyStatistic(models.Model):
    date = models.DateField(auto_now_add=True)

    number_checks = models.IntegerField()
    number_non_aborted_checks = models.IntegerField()
    number_erroneous_checks = models.IntegerField()
    average_check_duration = models.FloatField()

    number_transactions = models.IntegerField()
    number_successful_transactions = models.IntegerField()
    number_transactions_with_error = models.IntegerField()
    number_transactions_not_found = models.IntegerField()
    total_delta_profit_in_transactions = models.FloatField()
    average_delta_profit_in_transactions = models.FloatField()
    total_forecasted_profit_in_transactions = models.FloatField()
    average_forecasted_profit_in_transactions = models.FloatField()
    count_positive_transactions = models.IntegerField()
    amount_positive_transactions = models.FloatField()
    count_negative_transactions = models.IntegerField()
    amount_negative_transactions = models.FloatField()
    total_fees = models.FloatField()

    apr = models.FloatField()
    daily_apr = models.FloatField()
    weekly_apr = models.FloatField()
    weekly_profit = models.FloatField()

    total_owned = models.FloatField()
