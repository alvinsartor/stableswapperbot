# Django
from django.contrib import admin

# Local
from bot.models import Check
from bot.models import DailyStatistic
from bot.models import JournalEntry


@admin.register(Check)
class CheckAdmin(admin.ModelAdmin):
    list_display = (
        'date',
        'token_to_swap_from',
        'token_to_swap_to',
        'forecasted_profit',
        'aborted',
        'has_error',
        'duration_seconds',
    )

    def has_error(self, check):
        return bool(check.error)


@admin.register(DailyStatistic)
class DailyStatisticAdmin(admin.ModelAdmin):
    list_display = ('id', 'date')


@admin.register(JournalEntry)
class JournalEntryAdmin(admin.ModelAdmin):
    list_display = ('date', 'goal', 'title')
