
# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.db import migrations, models

def profit(tx):
    if not tx.total_owned_at_confirmation or not tx.total_owned_at_creation:
        return 0
    return tx.total_owned_at_confirmation - tx.total_owned_at_creation

def fill_past_statistics(apps, schema_editor):
    DailyStatistic = apps.get_model('bot', 'DailyStatistic')
    Transaction = apps.get_model('kernel', 'Transaction')       
    
    for daily_statistic in DailyStatistic.objects.all():
        transactions = Transaction.objects.filter(date__date=daily_statistic.date)
        
        positive_transactions = [tx for tx in transactions if profit(tx) > 0]
        daily_statistic.count_positive_transactions = len(positive_transactions)
        daily_statistic.amount_positive_transactions = sum([profit(tx) for tx in positive_transactions])
        
        negative_transactions = [tx for tx in transactions if profit(tx) < 0]
        daily_statistic.count_negative_transactions = len(negative_transactions)
        daily_statistic.amount_negative_transactions = -sum([profit(tx) for tx in negative_transactions])
        daily_statistic.save()


class Migration(migrations.Migration):

    dependencies = [('bot', '0014_past_statistics_datamigration')]
    operations = [migrations.RunPython(fill_past_statistics)]
