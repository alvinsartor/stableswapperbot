# Generated by Django 3.2.4 on 2021-06-30 15:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0003_ownedamount'),
    ]

    operations = [
        migrations.AddField(
            model_name='check',
            name='total_owned',
            field=models.FloatField(null=True),
        ),
    ]
