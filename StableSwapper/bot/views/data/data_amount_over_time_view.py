# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from kernel.models import Transaction


class DataAmountOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d7_ago = timezone.now() - timedelta(days=7)
        amount_over_time = Transaction.objects.filter(
            total_owned_at_confirmation__isnull=False, date__gte=d7_ago, state=Transaction.States.CONFIRMED
        ).order_by('-date')

        data = list(amount_over_time.values_list('total_owned_at_confirmation', flat=True))
        labels = [date.strftime("%d/%m %H:%M") for date in amount_over_time.values_list('date', flat=True)]

        return JsonResponse({'data': data, 'labels': labels})
