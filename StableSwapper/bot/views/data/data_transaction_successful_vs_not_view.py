# Python
from datetime import timedelta
from typing import List

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import DailyStatistic


class DataTransactionSuccessfulVsNotView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        stats = DailyStatistic.objects.filter(date__gte=d30_ago).order_by('date')

        percentage_successful: List[float] = []
        percentage_not_found: List[float] = []
        percentage_failed: List[float] = []

        for stat in stats:
            tot_transactions = stat.number_transactions
            percentage_successful.append(stat.number_successful_transactions / tot_transactions)
            percentage_not_found.append(stat.number_transactions_not_found / tot_transactions)
            percentage_failed.append(stat.number_transactions_with_error / tot_transactions)

        labels = [date.strftime("%d/%m %H:%M") for date in stats.values_list('date', flat=True)]

        data = {
            'percentage_successful': percentage_successful,
            'percentage_not_found': percentage_not_found,
            'percentage_failed': percentage_failed,
            'labels': labels,
        }
        return JsonResponse(data)
