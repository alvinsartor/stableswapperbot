# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import DailyStatistic


class DataDailyAprOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        apy_over_time = DailyStatistic.objects.filter(date__gte=d30_ago).order_by('-date')

        data = list(apy_over_time.values_list('daily_apr', flat=True))
        labels = [date.strftime("%d/%m %H:%M") for date in apy_over_time.values_list('date', flat=True)]

        return JsonResponse({'data': data, 'labels': labels})
