# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import DailyStatistic


class DataCheckTrendsOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        stats = DailyStatistic.objects.filter(date__gte=d30_ago).order_by('date')

        data_total = list(stats.values_list('number_checks', flat=True))
        data_successful = list(stats.values_list('number_non_aborted_checks', flat=True))
        labels = [date.strftime("%d/%m %H:%M") for date in stats.values_list('date', flat=True)]

        return JsonResponse({'data_total': data_total, 'data_successful': data_successful, 'labels': labels})
