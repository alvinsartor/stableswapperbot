# Python
from datetime import timedelta
from typing import List
from datetime import timedelta, datetime
import itertools

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import Check


class DataFailuresOverTimeView(View):
    def get(self, request, *args, **kwargs):
        h24_ago = timezone.now() - timedelta(days=1)
        checks = Check.objects.filter(forecasted_profit__isnull=False, date__gte=h24_ago).order_by('-date')

        groups = itertools.groupby(checks, lambda c: c.date.strftime("%d/%m %H"))

        data: List[float] = []
        labels: List[str] = []

        for group, checks in groups:
            labels.append(group)
            data.append(sum([1 if check.error else 0 for check in checks]))

        return JsonResponse({'data': data, 'labels': labels})
