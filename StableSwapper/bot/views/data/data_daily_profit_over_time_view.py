# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import DailyStatistic


class DataDailyProfitOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d31_ago = timezone.now() - timedelta(days=31)
        last_month_statistics = DailyStatistic.objects.filter(date__gte=d31_ago).order_by('date')

        total_owned = list(last_month_statistics.values_list('total_owned', flat=True))
        profits = []
        for i in range(1, len(total_owned)):
            profits.append(total_owned[i] - total_owned[i - 1])

        labels = [date.strftime("%d/%m %H:%M") for date in last_month_statistics.values_list('date', flat=True)][1:]

        return JsonResponse({'data': profits, 'labels': labels})
