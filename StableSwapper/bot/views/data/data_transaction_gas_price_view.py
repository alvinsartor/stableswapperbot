# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from kernel.models import Transaction


class DataTransactionGasPriceView(View):
    def get(self, request, *args, **kwargs):
        d7_ago = timezone.now() - timedelta(days=7)
        transactions = Transaction.objects.filter(gas_price__isnull=False, date__gte=d7_ago).order_by('date')

        data = list(transactions.values_list('gas_price', flat=True))
        labels = [date.strftime("%d/%m %H:%M") for date in transactions.values_list('date', flat=True)]

        return JsonResponse({'data': data, 'labels': labels})
