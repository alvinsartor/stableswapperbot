# Python
from typing import List
from datetime import timedelta
import itertools
from statistics import mean

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import Check


class DataProfitOverTimeView(View):
    def get(self, request, *args, **kwargs):
        h24_ago = timezone.now() - timedelta(days=1)
        checks = Check.objects.filter(forecasted_profit__isnull=False, date__gte=h24_ago).order_by('-date')

        groups = itertools.groupby(checks, lambda c: c.date.strftime("%d/%m %H"))

        data: List[float] = []
        labels: List[str] = []

        for group, checks in groups:
            labels.append(group)
            all_forecasted_profits = [check.forecasted_profit for check in checks]
            data.append(mean(all_forecasted_profits or [0]))

        return JsonResponse({'data': data, 'labels': labels})
