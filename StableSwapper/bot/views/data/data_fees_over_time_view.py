# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import DailyStatistic


class DataFeesOverTimeView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        check_duration = DailyStatistic.objects.filter(date__gte=d30_ago).order_by('date')

        data = list(check_duration.values_list('total_fees', flat=True))
        labels = [date.strftime("%d/%m %H:%M") for date in check_duration.values_list('date', flat=True)]

        return JsonResponse({'data': data, 'labels': labels})
