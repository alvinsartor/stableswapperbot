# Python
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import DailyStatistic


class DataTransactionPositiveVsNegativeCountView(View):
    def get(self, request, *args, **kwargs):
        d30_ago = timezone.now() - timedelta(days=30)
        stats = DailyStatistic.objects.filter(date__gte=d30_ago).order_by('date')

        data_positive = list(stats.values_list('count_positive_transactions', flat=True))
        data_negative = list(stats.values_list('count_negative_transactions', flat=True))
        labels = [date.strftime("%d/%m %H:%M") for date in stats.values_list('date', flat=True)]

        return JsonResponse({'data_positive': data_positive, 'data_negative': data_negative, 'labels': labels})
