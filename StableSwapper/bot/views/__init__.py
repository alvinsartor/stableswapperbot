
from .home_view import HomeView as HomeView
from .checks_view import ChecksView as ChecksView
from .transactions_view import TransactionsView as TransactionsView  
from .statistics_view import StatisticsView as StatisticsView
