# Django
from django.views.generic import ListView

# Local
from kernel.models import Transaction


class TransactionsView(ListView):
    template_name = 'bot/page-transactions.html'
    model = Transaction
    context_object_name = 'transactions'
    queryset = Transaction.objects.all().select_related('token_from', 'token_to').order_by('-date')
    paginate_by = 120
