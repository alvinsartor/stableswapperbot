# Django
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.core.mail import send_mail
from django.conf import settings


class TestMailTriggerView(View):
    def get(self, request, *args, **kwargs):
        send_mail(
            'Just a test!',
            'Nothing to worry about. We are Just testing the emails',
            None,
            [settings.DEFAULT_ADMIN_EMAIL],
            fail_silently=False,
        )
        return JsonResponse({'outcome': 'Email sent!'})
