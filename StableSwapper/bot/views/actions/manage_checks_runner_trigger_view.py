# Django
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from wallet.checks_runner import RunnersManager


class ManageChecksRunnerTriggerView(View):
    def get(self, request, *args, **kwargs):

        # We gently ask the runner to shutdown, but if they do not respond
        # we can safely assume they're dead. In that case we just kill their session.
        RunnersManager.close_sessions_for_runners_that_are_gone_awol()

        # If the runner is idle, shut it down and wait for the next iteration
        if RunnersManager.is_runner_idle():
            RunnersManager.ask_current_runner_to_shutdown()
            return JsonResponse({'outcome': 'shutdown', 'message': 'Runner was idle so it has been asked to shutdown'})

        # If the runner is not idle and already running (healty), we just leave the situation as it is
        if RunnersManager.is_runner_active():
            return JsonResponse({'outcome': 'skipped', 'message': 'Runner is already alive'})

        # If no runner is started, we start one
        RunnersManager.start_new_runner()
        return JsonResponse({'outcome': 'started', 'message': 'A new runner has been started'})
