# Django
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.core.mail import send_mail
from django.conf import settings

# Local
from wallet.wallet_checker import WalletChecker
from wallet.swapper_v3 import SwapperV3


class AlertIfLowInMaticTriggerView(View):
    def get(self, request, *args, **kwargs):
        balance = WalletChecker.get_matic_balance()
        if balance > 0.2:
            return JsonResponse({'outcome': 'enough funds in the wallet', 'balance': balance})

        top_up_tx_address = SwapperV3.top_up_MATIC()

        mail_text = 'StableSwapper only got %s MATIC left in the wallet.\n\n' % balance
        if top_up_tx_address:
            mail_subject = 'StableSwapper has been topped up'
            mail_text += 'A top-up has been succesfully triggered!\n'
            mail_text += 'Check it out here: https://polygonscan.com/tx/%s' % top_up_tx_address
        else:
            mail_subject = 'StableSwapper top-up failed'
            mail_text += ' A top-up was triggered but failed! WE NEED HUMAN HELP!'

        send_mail(mail_subject, mail_text, None, [settings.DEFAULT_ADMIN_EMAIL], fail_silently=False)
        return JsonResponse({'outcome': 'a top-up has been triggered. Tx: %s' % top_up_tx_address, 'balance': balance})
