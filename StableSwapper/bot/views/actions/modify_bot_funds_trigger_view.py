# Django
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.utils import timezone

# Local
from kernel.models import Transaction
from bot.models import Check
from bot.models import DailyStatistic
from bot.models import JournalEntry


class ModifyBotFundsTriggerView(View):
    """
        Endpoint used to modify in bulk all past transactions, checks and statistics
        whenever new funds are added to the bot. This is useful to not skew the stats.
    """

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return JsonResponse({'outcome': 'failure', 'message': 'user not authenticated'})

        # Retrieve amount that will be used to update past data
        try:
            amount = int(kwargs['amount'])
        except ValueError:
            return JsonResponse({'outcome': 'failure', 'message': 'user not authenticated'})

        # Update transactions
        all_transactions = Transaction.objects.all()
        for transaction in all_transactions:
            if transaction.total_owned_at_creation:
                transaction.total_owned_at_creation += amount
            if transaction.total_owned_at_confirmation:
                transaction.total_owned_at_confirmation += amount
        Transaction.objects.bulk_update(all_transactions, ['total_owned_at_creation', 'total_owned_at_confirmation'])

        # Update checks
        all_checks = Check.objects.all()
        for check in all_checks:
            if check.total_owned:
                check.total_owned += amount
        Check.objects.bulk_update(all_checks, ['total_owned'])

        # Update statistics
        all_stats = DailyStatistic.objects.all()
        for stat in all_stats:
            if stat.total_owned:
                stat.total_owned += amount
        DailyStatistic.objects.bulk_update(all_stats, ['total_owned'])

        # Create journal entry
        journal_entry = JournalEntry()
        journal_entry.date = timezone.now().date()
        journal_entry.text = "Retroactively updated transactions, checks and statistics by %s" % amount
        journal_entry.save()

        return JsonResponse({'outcome': 'success', 'message': 'owned amount has been updated as requested'})
