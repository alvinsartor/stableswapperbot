# Django
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from wallet.swapper_v3 import SwapperV3


class SwapTriggerView(View):
    def get(self, request, *args, **kwargs):
        outcome = SwapperV3.swap_if_possible()
        if not outcome:
            outcome = SwapperV3.swap_if_possible()

        return JsonResponse({'success': outcome})
