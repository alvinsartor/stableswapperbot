# Local
from datetime import timedelta

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View

# Local
from bot.models import Check
from kernel.models import Transaction


class OldChecksDeleteTriggerView(View):
    def get(self, request, *args, **kwargs):
        ids_of_checks_to_delete = Check.objects.all().order_by('-date').values_list('id', flat=True)[6000:]
        checks_to_delete = Check.objects.filter(id__in=ids_of_checks_to_delete)
        checks_count = checks_to_delete.count()
        checks_to_delete.delete()

        one_week_ago = timezone.now() - timedelta(days=31)
        transactions_to_delete = Transaction.objects.filter(date__lte=one_week_ago)
        txs_count = transactions_to_delete.count()
        transactions_to_delete.delete()

        return JsonResponse({'outcome': 'success', 'deleted_checks': checks_count, 'deleted_transactions': txs_count})
