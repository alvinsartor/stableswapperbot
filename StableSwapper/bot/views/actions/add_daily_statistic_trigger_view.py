# Python
from statistics import mean

# Django
from django.utils import timezone
from django.http.response import JsonResponse
from django.views.generic.base import View
from django.forms.models import model_to_dict

# Local
from bot.views import HomeView
from bot.models import DailyStatistic
from bot.models import Check
from kernel.models import Transaction


class AddDailyStatisticTriggerView(View):
    def get(self, request, *args, **kwargs):

        midnight_of_today = timezone.now().date()
        statistic = DailyStatistic()

        # Interests & Profit statistics
        home_page_statistics = HomeView.get_statistics()
        statistic.apr = home_page_statistics['APR']
        statistic.daily_apr = home_page_statistics['daily_APR']
        statistic.weekly_apr = home_page_statistics['profit_perc']
        statistic.weekly_profit = home_page_statistics['profit_abs']
        statistic.total_owned = home_page_statistics['total_owned']

        # Checks statistics
        last_24h_checks = Check.objects.filter(date__gte=midnight_of_today)
        statistic.number_checks = last_24h_checks.count()
        statistic.number_non_aborted_checks = last_24h_checks.filter(aborted=False).count()
        statistic.number_erroneous_checks = last_24h_checks.filter(error__isnull=False).count()
        all_durations = list(last_24h_checks.values_list('duration_seconds', flat=True))
        statistic.average_check_duration = mean(all_durations or [0])

        # Transactions statistics
        last_24h_txs = Transaction.objects.filter(date__gte=midnight_of_today)
        statistic.number_transactions = last_24h_txs.count()
        statistic.number_successful_transactions = last_24h_txs.filter(state=Transaction.States.CONFIRMED).count()
        statistic.number_transactions_with_error = last_24h_txs.filter(state=Transaction.States.FAILED).count()
        statistic.number_transactions_not_found = last_24h_txs.filter(state=Transaction.States.NOT_FOUND).count()
        statistic.total_fees = sum([tx.fee_in_MATIC for tx in last_24h_txs])

        confirmed_transactions = last_24h_txs.filter(state=Transaction.States.CONFIRMED)
        statistic.total_delta_profit_in_transactions = sum(map(lambda tx: tx.profit, confirmed_transactions))
        statistic.average_delta_profit_in_transactions = (
            statistic.total_delta_profit_in_transactions / statistic.number_successful_transactions
            if statistic.number_successful_transactions
            else 0
        )
        statistic.total_forecasted_profit_in_transactions = sum(
            map(lambda tx: tx.forecasted_profit, confirmed_transactions)
        )
        statistic.average_forecasted_profit_in_transactions = (
            statistic.total_forecasted_profit_in_transactions / statistic.number_successful_transactions
            if statistic.number_successful_transactions
            else 0
        )

        positive_transactions = [tx for tx in confirmed_transactions if tx.profit > 0]
        statistic.count_positive_transactions = len(positive_transactions)
        statistic.amount_positive_transactions = sum([tx.profit for tx in positive_transactions])

        negative_transactions = [tx for tx in confirmed_transactions if tx.profit < 0]
        statistic.count_negative_transactions = len(negative_transactions)
        statistic.amount_negative_transactions = -sum([tx.profit for tx in negative_transactions])

        statistic.save()

        # Delete Pevious statistics referring to the same date
        DailyStatistic.objects.filter(date=statistic.date).exclude(id=statistic.id).delete()

        return JsonResponse(model_to_dict(statistic))
