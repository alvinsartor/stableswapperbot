# Python
from typing import Dict, Any, List, Tuple
import json

# Django
from django.views.generic.base import TemplateView
from django.db.models import Count

# 3rd Party
import requests

# Local
from wallet.wallet_checker import WalletChecker
from bot.models import Check
from bot.models import DailyStatistic
from bot.models import JournalEntry


class StatisticsView(TemplateView):
    template_name = 'bot/page-statistics.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['todays_statistics'] = DailyStatistic.objects.last()
        context['latest_entries'] = JournalEntry.objects.all().order_by('-date')[:12]
        context['common_errors'] = self._get_common_errors()
        context['network_statistics'] = self._get_network_statistics()
        context['matic_balance'] = WalletChecker.get_matic_balance()

        return context

    def _get_common_errors(self) -> List[Tuple[str, int]]:
        checks_with_error = (
            Check.objects.filter(error__isnull=False).values('error').annotate(count=Count('error')).order_by('-count')
        )

        return [[entry['error'], entry['count']] for entry in checks_with_error][:12]

    def _get_network_statistics(self) -> Dict[str, Any]:
        response = requests.get('https://gasstation-mainnet.matic.network').content
        decoded = json.loads(response)
        return decoded
