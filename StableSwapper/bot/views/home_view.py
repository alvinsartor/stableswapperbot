# Local
import math
from typing import Dict, Any
from datetime import timedelta

# Django
from django.utils import timezone
from django.views.generic.base import TemplateView

# Local
from wallet.wallet_checker import WalletChecker
from kernel.models import Transaction
from bot.models import Check


class HomeView(TemplateView):
    template_name = 'bot/page-home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(HomeView.get_statistics())

        context['latest_transactions'] = Transaction.objects.all().order_by('-date')[:20]
        context['nr_transactions'] = Transaction.objects.all().count()

        context['latest_checks'] = Check.objects.all().order_by('-date')[:19]
        context['nr_checks'] = Check.objects.all().count()

        return context

    @staticmethod
    def get_statistics() -> Dict[str, Any]:
        context = {}

        context['owned_tokens'] = sorted(
            WalletChecker.get_tokens_amounts(), key=lambda tk: tk.adjusted_amount, reverse=True
        )
        context['total_owned'] = sum([token.adjusted_amount for token in context['owned_tokens']])

        try:
            last_week = timezone.now() - timedelta(days=7)
            tx = (
                Transaction.objects.filter(
                    total_owned_at_creation__isnull=False, date__gte=last_week, state=Transaction.States.CONFIRMED
                )
                .order_by('date')
                .first()
            )
            context['initial'] = tx.total_owned_at_creation
            context['initial_date'] = tx.date
        except AttributeError:
            context['initial'] = context['total_owned']
            context['initial_date'] = timezone.now()

        context['profit_abs'] = context['total_owned'] - context['initial']
        context['profit_perc'] = 100 * context['profit_abs'] / context['initial']

        days_since_initial: float = (
            timezone.now() - context['initial_date']
        ).total_seconds() / 86400  # should be 7, but will be less if there is not enough data
        context['daily_APR'] = context['profit_perc'] / days_since_initial if days_since_initial else 0.0
        context['APR'] = context['daily_APR'] * 365
        try:
            context['APY'] = (math.pow(1 + context['daily_APR'] / 100, 365) - 1) * 100
        except Exception:
            context['APY'] = float("inf")

        return context
