# Django
from django.views.generic import ListView

# Local
from bot.models import Check


class ChecksView(ListView):
    template_name = 'bot/page-checks.html'
    model = Check
    context_object_name = 'checks'
    queryset = Check.objects.all().select_related('token_to_swap_from', 'token_to_swap_to').order_by('-date')
    paginate_by = 120
