
async function plotAmountChart()
{
    const canvas = document.getElementById('amount-chart');
    const rawData = await $.getJSON('/data/amount_over_time/');
    
    Chart.defaults.plugins.legend.display = false;
    Chart.defaults.responsive = true;
    Chart.defaults.maintainAspectRatio = false;

    new Chart(canvas, {
        type: 'line',
        data: {
            labels: rawData.labels.reverse(),
            datasets: [{
                data: rawData.data.reverse(),
                fill: false,
                borderColor: 'rgb(4, 167, 119)',
                tension: 0.1
            }],
        },        
    });
} 

plotAmountChart();