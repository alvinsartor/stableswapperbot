
const originalTitle = document.title;
const reloadFrequencyInSeconds = 75;
const updateFrequencyInSeconds = 5;

function updateCounter(remaining)
{
    document.title = originalTitle + " - " + remaining + "s";
    setTimeout(() => { updateCounter(remaining - updateFrequencyInSeconds); }, updateFrequencyInSeconds * 1000);
}

function reloadInFiveMinutes()
{
    setTimeout(() => { window.location.reload(); }, reloadFrequencyInSeconds * 1000);
    updateCounter(reloadFrequencyInSeconds)
}

reloadInFiveMinutes();