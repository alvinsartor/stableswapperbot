# Python
import random
from datetime import timedelta

# Django
from django.utils import timezone

# 3rd Party
import factory
from factory import fuzzy

# Local
from kernel.models import Token
from kernel.factories import TransactionFactory
from bot.models import Check
from bot.models import DailyStatistic
from bot.models import JournalEntry

POSSIBLE_ERRORS = ['Something bad happened', 'Whoops, unknown', 'XYZ not recognized', 'Cannot estimate']


class CheckFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Check

    token_to_swap_from = factory.LazyAttribute(lambda x: Token.objects.all().order_by('?')[0])
    token_to_swap_to = factory.LazyAttribute(
        lambda x: Token.objects.exclude(symbol=x.token_to_swap_from.symbol).order_by('?')[0]
    )
    amount_to_swap_from = fuzzy.FuzzyFloat(100, high=150)
    amount_to_swap_to = factory.LazyAttribute(lambda x: x.amount_to_swap_from + random.uniform(0.1, 0.25))
    forecasted_profit = factory.LazyAttribute(lambda x: x.amount_to_swap_to - x.amount_to_swap_from)
    aborted = fuzzy.FuzzyChoice([True, True, True, True, True, False])
    transaction = factory.LazyAttribute(
        lambda x: None
        if x.aborted
        else TransactionFactory(
            token_from=x.token_to_swap_from,
            token_to=x.token_to_swap_to,
            amount_from=x.amount_to_swap_from,
            amount_to=x.amount_to_swap_to,
        )
    )
    error = factory.LazyAttribute(lambda x: None if random.uniform(0, 1) > 0.1 else random.choice(POSSIBLE_ERRORS))
    duration_seconds = fuzzy.FuzzyFloat(2, high=5.5)
    total_owned = fuzzy.FuzzyFloat(450, high=700)


class DailyStatisticFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DailyStatistic

    number_checks = fuzzy.FuzzyInteger(2000, high=3000)
    number_non_aborted_checks = fuzzy.FuzzyInteger(100, high=200)
    number_erroneous_checks = fuzzy.FuzzyInteger(50, high=250)
    average_check_duration = fuzzy.FuzzyFloat(2.0, high=15.0)

    number_transactions = fuzzy.FuzzyInteger(50, high=300)
    number_successful_transactions = fuzzy.FuzzyInteger(40, high=150)
    number_transactions_with_error = fuzzy.FuzzyInteger(40, high=150)
    number_transactions_not_found = fuzzy.FuzzyInteger(20, high=100)
    total_delta_profit_in_transactions = fuzzy.FuzzyFloat(2.0, high=15.0)
    average_delta_profit_in_transactions = factory.LazyAttribute(
        lambda x: x.total_delta_profit_in_transactions / x.number_successful_transactions
    )
    total_forecasted_profit_in_transactions = fuzzy.FuzzyFloat(3.0, high=25.0)
    average_forecasted_profit_in_transactions = factory.LazyAttribute(
        lambda x: x.total_forecasted_profit_in_transactions / x.number_successful_transactions
    )

    count_positive_transactions = fuzzy.FuzzyInteger(25, high=125)
    amount_positive_transactions = fuzzy.FuzzyFloat(5.0, high=32.0)
    count_negative_transactions = fuzzy.FuzzyInteger(0, high=25)
    amount_negative_transactions = fuzzy.FuzzyFloat(0.0, high=16.0)
    total_fees = fuzzy.FuzzyFloat(1, high=3.0)

    apr = factory.LazyAttribute(lambda x: x.daily_apr * 365)
    daily_apr = fuzzy.FuzzyFloat(0.25, high=1.5)
    weekly_apr = factory.LazyAttribute(lambda x: x.daily_apr * 7)
    weekly_profit = fuzzy.FuzzyFloat(1000, high=1500)

    total_owned = fuzzy.FuzzyFloat(500, high=775)


POSSIBLE_GOALS = ['General Improvement', 'Reduce Errors', 'Increase Happiness']


class JournalEntryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = JournalEntry

    date = fuzzy.FuzzyDate(timezone.now().date() - timedelta(days=30))
    title = factory.Faker('sentence')
    goal = fuzzy.FuzzyChoice(POSSIBLE_GOALS)
    additional_info = factory.Faker('sentence')
