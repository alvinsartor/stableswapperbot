# Python
import random
from datetime import timedelta

# Django
from django.utils import timezone

# 3rd Party
import factory
from factory import fuzzy

# Local
from kernel.models import Token
from kernel.models import Transaction


class TokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Token

    symbol = fuzzy.FuzzyText(length=4)
    decimals = fuzzy.FuzzyChoice([6, 18])
    address = fuzzy.FuzzyText(length=32, prefix='0x')
    logo_uri = factory.Faker('url')


class TransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Transaction

    token_from = factory.LazyAttribute(lambda x: Token.objects.all().order_by('?')[0])
    token_to = factory.LazyAttribute(lambda x: Token.objects.exclude(symbol=x.token_from.symbol).order_by('?')[0])
    amount_from = fuzzy.FuzzyFloat(100, high=150)
    amount_to = factory.LazyAttribute(lambda x: x.amount_from + 0.15)
    state = fuzzy.FuzzyChoice(['W', 'C', 'C', 'C', 'F', 'U', 'N'])
    total_owned_at_creation = fuzzy.FuzzyFloat(450, high=550)
    total_owned_at_confirmation = factory.LazyAttribute(
        lambda x: x.total_owned_at_creation + random.uniform(-0.25, 0.75)
    )
    gas_price = fuzzy.FuzzyInteger(1000000000, high=9000000000)
    slippage = fuzzy.FuzzyFloat(0, high=1)
    block = factory.Sequence(int)
    execution_date = factory.LazyAttribute(lambda x: timezone.now() + timedelta(seconds=random.randrange(1, 25)))
