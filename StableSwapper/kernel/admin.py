# Django
from django.contrib import admin

# Local
from kernel.models import Token, Transaction


@admin.register(Token)
class TokenAdmin(admin.ModelAdmin):
    list_display = ('symbol', 'disabled', 'maximum_risk_allowed', 'address', 'comment')


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('date', 'total_owned_at_creation', 'state', 'token_from', 'token_to', 'amount_from', 'amount_to')
    list_filter = ('state',)
