# Django
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Token(models.Model):
    symbol = models.CharField(max_length=16, blank=False, null=False, unique=True)
    decimals = models.IntegerField()
    address = models.CharField(max_length=64, blank=False, null=False)
    logo_uri = models.CharField(max_length=512, blank=True, default='')
    disabled = models.BooleanField(default=False)
    comment = models.CharField(max_length=256, blank=True, default='')
    maximum_risk_allowed = models.FloatField(
        default=1,
        validators=[MinValueValidator(0.0), MaxValueValidator(1.0)],
        help_text="Maximum percentage of the total amount we allow this token to get to",
    )

    def __str__(self):
        return self.symbol
