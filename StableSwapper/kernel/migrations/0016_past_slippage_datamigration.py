
# Python
from datetime import timedelta
import json

# Django
from django.utils import timezone
from django.db import migrations, models

def add_past_slippage(apps, schema_editor):
    Transaction = apps.get_model('kernel', 'Transaction')       
    
    transactions = Transaction.objects.filter(bot_check__isnull=False)

    for tx in transactions:       
        check_raw_data = tx.bot_check.raw_data
        if not check_raw_data:
            continue

        parsed_raw_data = json.loads(check_raw_data)
        slippage = parsed_raw_data.get('slippage', None)
            
        #Note: slippage can be 0, so checking for truthy is not enough
        if slippage is not None: 
            tx.slippage = slippage
            tx.save()

class Migration(migrations.Migration):

    dependencies = [('kernel', '0015_transaction_slippage')]
    operations = [migrations.RunPython(add_past_slippage)]
