# Generated by Django 3.2.5 on 2021-07-19 10:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kernel', '0014_auto_20210718_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='slippage',
            field=models.FloatField(default=None, null=True),
        ),
    ]
