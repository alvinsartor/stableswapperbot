# Generated by Django 3.2.4 on 2021-07-02 05:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kernel', '0006_alter_transaction_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='total_owned',
            field=models.FloatField(null=True),
        ),
    ]
