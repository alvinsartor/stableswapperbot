# Generated by Django 3.2.5 on 2021-07-18 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kernel', '0013_transaction_gas_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='block',
            field=models.IntegerField(default=None, null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='execution_date',
            field=models.DateTimeField(default=None, null=True),
        ),
    ]
