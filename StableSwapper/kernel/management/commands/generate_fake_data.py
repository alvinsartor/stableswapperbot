# Django
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

# Local
from kernel.models import Token
from kernel.models import Transaction
from kernel.factories import TokenFactory
from bot.factories import CheckFactory
from bot.models import DailyStatistic
from bot.factories import DailyStatisticFactory
from bot.models import JournalEntry
from bot.factories import JournalEntryFactory

#
# NOTE: To run management commands from the shell:
#
# from django.core import management; management.call_command('generate_fake_data')
#

class Command(BaseCommand):
    help = 'Resets the DB and generates fake data'

    def handle(self, *args, **options):
        if not settings.DEBUG:
            raise Exception("You cannot generate fake data in production!")
        
        self.stdout.write('Deleting existing data..')
        Transaction.objects.all().delete()
        Token.objects.all().delete()
        DailyStatistic.objects.all().delete()
        JournalEntry.objects.all().delete()

        self.stdout.write('Creating random tokens..')
        TokenFactory(symbol='USDC', address='0x2791bca1f2de4661ed88a30c99a7a9449aa84174', decimals=6, logo_uri='https://tokens.1inch.exchange/0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48.png')
        TokenFactory(symbol='USDT', address='0xc2132d05d31c914a87c6611c10748aeb04b58e8f', decimals=6, logo_uri='https://tokens.1inch.exchange/0xdac17f958d2ee523a2206206994597c13d831ec7.png')
        TokenFactory(symbol='DAI', address='0x8f3cf7ad23cd3cadbd9735aff958023239c6a063', decimals=18, logo_uri='https://tokens.1inch.exchange/0x6b175474e89094c44da98b954eedeac495271d0f.png')
        TokenFactory(symbol='MAI', address='0xa3fa99a148fa48d14ed51d610c367c61876997f1', decimals=18, logo_uri='https://tokens.1inch.exchange/0xa3fa99a148fa48d14ed51d610c367c61876997f1.png')
        TokenFactory(symbol='FRAX', address='0x104592a158490a9228070e0a8e5343b499e125d0', decimals=18, logo_uri='https://tokens.1inch.exchange/0x853d955acef822db058eb8505911ed77f175b99e.png')
        TokenFactory(symbol='mUSD', address='0xe840b73e5287865eec17d250bfb1536704b43b21', decimals=18, logo_uri='https://tokens.1inch.exchange/0xe2f2a5c287993345a840db3b0845fbc70f5935a5.png')

        self.stdout.write('Creating random checks and transactions..')
        CheckFactory.create_batch(1500)        
        
        self.stdout.write('Creating random statistics..')
        DailyStatisticFactory.create_batch(30)        
        
        self.stdout.write('Creating journal entries..')
        JournalEntryFactory.create_batch(30)        

        self.stdout.write('Successfully created fake data')