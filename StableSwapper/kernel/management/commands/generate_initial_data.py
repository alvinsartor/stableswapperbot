# Django
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User

# Local
from kernel.models import Token
from kernel.models import Transaction
from kernel.factories import TokenFactory
from kernel.factories import TransactionFactory

class Command(BaseCommand):
    help = 'Generates the initial data'

    def handle(self, *args, **options):
        if Token.objects.all().count() > 0:
            raise Exception("It seems that the data has already been generated")

        self.stdout.write('Creating superuser..')
        email = settings.DEFAULT_ADMIN_EMAIL
        password = settings.DEFAULT_ADMIN_PASSWORD
        user = User(username=email, email=email, is_superuser=True, is_staff=True)
        user.set_password(password)
        user.save()

        self.stdout.write('Creating tokens..')
        TokenFactory(symbol='USDC', address='0x2791bca1f2de4661ed88a30c99a7a9449aa84174', decimals=6, logo_uri="https://tokens.1inch.exchange/0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48.png")
        TokenFactory(symbol='USDT', address='0xc2132d05d31c914a87c6611c10748aeb04b58e8f', decimals=6, logo_uri="https://tokens.1inch.exchange/0xdac17f958d2ee523a2206206994597c13d831ec7.png")
        TokenFactory(symbol='DAI', address='0x8f3cf7ad23cd3cadbd9735aff958023239c6a063', decimals=18, logo_uri="https://tokens.1inch.exchange/0x6b175474e89094c44da98b954eedeac495271d0f.png")
        TokenFactory(symbol='miMATIC', address='0xa3fa99a148fa48d14ed51d610c367c61876997f1', decimals=18, logo_uri="https://tokens.1inch.exchange/0xa3fa99a148fa48d14ed51d610c367c61876997f1.png")

        self.stdout.write('Successfully generated initial data')